import { useNetwork } from "./utils/useNetwork"
import { BrowserRouter as Router, Switch, Route } from "react-router-dom"

import Layout from "./hoc/layout"
import Categories from "./pages/categories"
import Providers from "./pages/providers"
import { AppToast } from "./components/ui"

const App = () => {
  const isOnline = useNetwork()

  return (
    <Router>
      <div className="App">
        <Layout>
          { !isOnline && <AppToast /> }
          <Switch>
            <Route path="/" exact>
              <Categories />
            </Route>
            <Route path="/category/:id/providers" exact>
              <Providers />
            </Route>
          </Switch>
        </Layout>
      </div>
    </Router>
  )
}

export default App
