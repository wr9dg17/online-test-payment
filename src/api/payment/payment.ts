const baseURL = process.env.REACT_APP_BASE_URL

export const fetchCategories = () => {
  return fetch(`${baseURL}/payment/categories`)
    .then(res => res.json())
    .then(data => Promise.resolve(data))
    .catch(error => Promise.reject(error))
}

export const fetchCategoryProviders = (categoryId: string) => {
  return fetch(`${baseURL}/payment/categories/${categoryId}`)
    .then(res => res.json())
    .then(data => Promise.resolve(data))
    .catch(error => Promise.reject(error))
}

export const makeNewPayment = (formData: any) => {
  return fetch(`${baseURL}/payment/new`, {
    method: "POST", body: JSON.stringify(formData)
  })
    .then(res => res.json())
    .then(data => Promise.resolve(data))
    .catch(error => Promise.reject(error))
}
