import React from "react"
import Header from "../components/layout/Header"

import Container from "react-bootstrap/Container"
import Row from "react-bootstrap/Row"
import Col from "react-bootstrap/Col"

type LayoutProps = {
  children: React.ReactNode
}

const Layout = (props: LayoutProps) => (
  <>
    <Header />
    <main className="content">
      <Container>
        <Row>
          <Col>
            { props.children }
          </Col>
        </Row>
      </Container>
    </main>
  </>
)

export default Layout