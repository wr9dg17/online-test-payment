import { createServer, Model, hasMany, belongsTo, RestSerializer } from "miragejs"

export const makeServer = ({ environment = "test" } = {}) => {
  return createServer({
    environment,

    models: {
      category: Model.extend({
        providers: hasMany()
      }),

      provider: Model.extend({
        category: belongsTo(),
        fields: hasMany("customField")
      }),

      customField: Model.extend({
        fields: belongsTo("provider.fields")
      })
    },

    serializers: {
      category: RestSerializer.extend({
        include: ["providers"],
        embed: true
      }),

      provider: RestSerializer.extend({
        include: ["fields"],
        embed: true
      })
    },

    seeds(server) {
      server.create("category", {
        name: "Mobile",
        providers: [
          server.create("provider", {
            name: "Azercell",
            fields: [
              server.create("customField", {
                type: 1,
                name: "phone_number",
                label: "Phone Number"
              })
            ]
          }),
          server.create("provider", {
            name: "Bakcel",
            fields: [
              server.create("customField", {
                type: 1,
                name: "phone_number",
                label: "Phone Number"
              })
            ]
          }),
          server.create("provider", {
            name: "Nar",
            fields: [
              server.create("customField", {
                type: 1,
                name: "phone_number",
                label: "Phone Number"
              })
            ]
          }),
        ]
      });

      server.create("category", {
        name: "Utility services",
        providers: [
          server.create("provider", {
            name: "AzərSu",
            fields: [
              server.create("customField", {
                type: 4,
                name: "individual_type",
                label: "Appointment",
                options: [
                  { k: "1", v: "Individuals" },
                  { k: "2", v: "Commercial consumers" },
                ]
              }),
              server.create("customField", {
                type: 1,
                name: "subscriber_id",
                label: "Subscriber code"
              })
            ]
          }),
          server.create("provider", {
            name: "AzəriQaz",
            fields: [
              server.create("customField", {
                type: 4,
                name: "individual_type",
                label: "Appointment",
                options: [
                  { k: "1", v: "Individuals" },
                  { k: "2", v: "Commercial consumers" },
                ]
              }),
              server.create("customField", {
                type: 1,
                name: "subscriber_id",
                label: "Subscriber code"
              })
            ]
          }),
          server.create("provider", {
            name: "Azərİşıq",
            fields: [
              server.create("customField", {
                type: 4,
                name: "individual_type",
                label: "Appointment",
                options: [
                  { k: "1", v: "Individuals" },
                  { k: "2", v: "Commercial consumers" },
                ]
              }),
              server.create("customField", {
                type: 1,
                name: "subscriber_id",
                label: "Subscriber code"
              })
            ]
          }),
          server.create("provider", {
            name: "Azəristiliktəchizat",
            fields: [
              server.create("customField", {
                type: 1,
                name: "subscriber_id",
                label: "Subscriber code"
              })
            ]
          }),
        ]
      });

      server.create("category", {
        name: "Internet",
        providers: [
          server.create("provider", {
            name: "AileNet",
            fields: [
              server.create("customField", {
                type: 1,
                name: "subscriber_id",
                label: "Subscriber code"
              })
            ]
          }),
          server.create("provider", {
            name: "Aztelecom",
            fields: [
              server.create("customField", {
                type: 1,
                name: "subscriber_id",
                label: "Subscriber code"
              })
            ]
          }),
          server.create("provider", {
            name: "CityNet",
            fields: [
              server.create("customField", {
                type: 1,
                name: "subscriber_id",
                label: "Subscriber code"
              })
            ]
          }),
          server.create("provider", {
            name: "Izone",
            fields: [
              server.create("customField", {
                type: 1,
                name: "subscriber_id",
                label: "Subscriber code"
              })
            ]
          }),
          server.create("provider", {
            name: "Teleport",
            fields: [
              server.create("customField", {
                type: 1,
                name: "subscriber_id",
                label: "Subscriber code"
              })
            ]
          })
        ]
      });

      server.create("category", {
        name: "TV",
        providers: [
          server.create("provider", {
            name: "AileTV",
            fields: [
              server.create("customField", {
                type: 1,
                name: "subscriber_id",
                label: "Subscriber code"
              })
            ]
          }),
          server.create("provider", {
            name: "ConnectTV",
            fields: [
              server.create("customField", {
                type: 1,
                name: "subscriber_id",
                label: "Subscriber code"
              })
            ]
          }),
          server.create("provider", {
            name: "KATV",
            fields: [
              server.create("customField", {
                type: 1,
                name: "subscriber_id",
                label: "Subscriber code"
              })
            ]
          }),
          server.create("provider", {
            name: "CityLineTV",
            fields: [
              server.create("customField", {
                type: 1,
                name: "subscriber_id",
                label: "Subscriber code"
              })
            ]
          }),
          server.create("provider", {
            name: "SmartTV",
            fields: [
              server.create("customField", {
                type: 1,
                name: "subscriber_id",
                label: "Subscriber code"
              })
            ]
          })
        ]
      });

      server.create("category", {
        name: "Landline",
        providers: [
          server.create("provider", {
            name: "Aztelekom/Baktelecom",
            fields: [
              server.create("customField", {
                type: 4,
                name: "city_id",
                label: "Region",
                options: [
                  { k: "1", v: "Bakı (12)" },
                  { k: "2", v: "Sumqayıt (18)" },
                  { k: "3", v: "Bərdə (2020)" },
                  { k: "4", v: "Ucar (2021)" },
                  { k: "5", v: "Ağsu (2022)" },
                  { k: "6", v: "Ağdaş (2023)" },
                  { k: "7", v: "Qobustan (2024)" },
                  { k: "8", v: "Kürdəmir (2025)" },
                  { k: "9", v: "Şamaxı (2026)" },
                  { k: "10", v: "Göyçay (2027)" },
                  { k: "11", v: "İsmayıllı (2028)" },
                  { k: "12", v: "Zərdab (2029)" },
                  { k: "13", v: "Hacıqabul (2030)" },
                ]
              }),
              server.create("customField", {
                type: 1,
                name: "phone_number",
                label: "Phone number"
              })
            ]
          })
        ]
      });
    },

    routes() {
      this.namespace = "api"

      this.get("/payment/categories", (schema) => {
        return schema.categories.all()
      })

      this.get("/payment/categories/:id", (schema, request) => {
        const id = request.params.id
        return schema.categories.find(id)
      })

      this.post("/payment/new", (schema, request) => {
        const { providerId, fields, amount } = JSON.parse(request.requestBody)
        const { attrs } = schema.providers.find(providerId)
        const response = {
          id: 1,
          date: new Date(),
          amount: amount,
          details: [
            { k: "Service", v: attrs.name }
          ]
        }

        Object.keys(fields).forEach(field => {
          response.details.push({
            k: field,
            v: fields[field]
          })
        })

        return response
      })
    }

  })
}