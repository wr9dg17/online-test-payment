import { useState, useEffect } from "react"
import { useParams } from "react-router-dom"
import { Category, Provider, Receipt } from "../shared/interfaces"
import { paymentAPI } from "../api"

import { Spinner, ProvidersList } from "../components/ui"
import PaymentForm from "../components/pages/providers/PaymentForm"
import PaymentResult from "../components/pages/providers/PaymentResult"

import Row from "react-bootstrap/Row"
import Col from "react-bootstrap/Col"

const Providers = () => {
  const { id }: any = useParams()
  const [loading, setLoading] = useState<boolean>(true)
  const [category, setCategory] = useState<Category>({} as Category)
  const [selectedProvider, setSelectedProvider] = useState<Provider>({} as Provider)
  const [paymentResult, setPaymentResult] = useState<Receipt>({} as Receipt)

  useEffect(() => {
    paymentAPI.fetchCategoryProviders(id)
      .then(data => {
        setCategory(data.category)
        setLoading(false)
      })

    return () => {
      setSelectedProvider({} as Provider)
    }
  }, [id])

  const selectProvider = (data: Provider) => {
    setSelectedProvider(data)
  }

  const handlePaymentSuccess = (data: Receipt) => {
    setPaymentResult(data)
  }

  return (
    <>
      { loading ? (
        <Spinner />
      ) : (
        <Row>
          <Col lg={3}>
            <ProvidersList
              providers={category.providers}
              onSelectProvider={(data) => selectProvider(data)}
            />
          </Col>
          <Col lg={6}>
            {paymentResult.id ? (
              <PaymentResult payment={paymentResult} />
            ) : (
              <PaymentForm
                providerName={selectedProvider.name}
                providerId={selectedProvider.id}
                fields={selectedProvider.fields}
                onPaymentSuccess={(data: Receipt) => handlePaymentSuccess(data)}
              />
            )}
          </Col>
        </Row>
      ) }
    </>
  )
}

export default Providers
