import { useState, useEffect } from "react"
import { useNetwork } from "../utils/useNetwork"
import { Category } from "../shared/interfaces"
import { paymentAPI } from "../api"

import { CategoryCard, Spinner } from "../components/ui"
import Row from "react-bootstrap/Row"
import Col from "react-bootstrap/Col"
import Button from "react-bootstrap/Button"

const Categories = () => {
  const isOnline = useNetwork();
  const [loading, setLoading] = useState<boolean>(true)
  const [categories, setCategories] = useState<Category[]>([])

  useEffect(() => {
    if (isOnline) {
      paymentAPI.fetchCategories()
        .then(data => {
          setLoading(false)
          setCategories(data.categories)
          localStorage.setItem("categories", JSON.stringify(data.categories))
        })
    } else {
      setLoading(false)
      const cachedCategories = localStorage.getItem("categories")
      cachedCategories && setCategories(JSON.parse(cachedCategories))
    }
  }, [isOnline])

  const reloadPage = (): void => {
    window.location.reload()
  }

  return (
    <>
      { loading ? (
        <Spinner />
      ) : (
        categories.length ? (
          <Row>
            { categories.map(category => (
              <Col lg={3} key={category.id}>
                <CategoryCard category={category} />
              </Col>
            )) }
          </Row>
        ) : (
          <div className="text-center">
            <Button variant="dark" onClick={reloadPage}>Tap to reload</Button>
          </div>
        )
      ) }
    </>
  )
}

export default Categories