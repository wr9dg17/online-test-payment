import Navbar from "react-bootstrap/Navbar"
import Container from "react-bootstrap/Container"
import { Link } from "react-router-dom"

const Header = () => (
  <Navbar bg="dark">
    <Container>
      <Navbar.Brand>
        <Link className="navbar-logo" to="/">Online payment</Link>
      </Navbar.Brand>
    </Container>
  </Navbar>
)

export default Header