import React from "react"
import { Link } from "react-router-dom"
import { Provider } from "../../../shared/interfaces"
import ListGroup from "react-bootstrap/ListGroup"

type ProvidersListProps = {
  providers: Provider[],
  onSelectProvider: (data: Provider) => void
}

const ProvidersList = ({ providers, onSelectProvider }: ProvidersListProps) => {
  return (
    <ListGroup>
      <ListGroup.Item active as={Link} to="/">Go back</ListGroup.Item>
      { providers.map(provider => (
        <ListGroup.Item
          action
          key={provider.id}
          onClick={() => onSelectProvider(provider)}
        >
          { provider.name }
        </ListGroup.Item>
      )) }
    </ListGroup>
  )
}

export default ProvidersList