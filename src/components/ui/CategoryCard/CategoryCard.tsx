import { Link } from "react-router-dom"
import Card from "react-bootstrap/Card"

type CategoryCardProps = {
  category: {
    id: number | string,
    name: string
  }
}

const CategoryCard = ({ category } : CategoryCardProps) => (
  <Card className="category-card">
    <Card.Body
      as={Link}
      to={`/category/${category.id}/providers`}
      className="category-card__body text-center"
    >
      {category.name}
    </Card.Body>
  </Card>
)

export default CategoryCard
