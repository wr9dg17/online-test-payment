import { useState } from "react"
import Toast from "react-bootstrap/Toast"

const AppToast = () => {
  const [show, setShow] = useState(true)

  return (
    <>
      { show ? (
        <Toast show={show} onClose={() => setShow(false)}>
          <Toast.Header>
            <strong className="me-auto">Connection error</strong>
          </Toast.Header>
          <Toast.Body>Pease check your internet connection!</Toast.Body>
        </Toast>
      ) : null }
    </>
  )
}

export default AppToast