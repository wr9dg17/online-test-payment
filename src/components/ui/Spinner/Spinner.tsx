import Spinner from "react-bootstrap/Spinner";

const Preloader = () => (
  <div className="spinner-container d-flex align-items-center justify-content-center">
    <Spinner animation="border" />
  </div>
)

export default Preloader