import { Field, useField } from "formik"
import { FieldProps } from "../../shared/types/fieldProps"

const TextField = (props: FieldProps) => {
  const [field, meta] = useField(props)

  return (
    <div className="form-group mb-2">
      { props.label && <label className="form-label">{ props.label }</label> }
      <Field
        {...field}
        {...props.rest}
        type={props.type}
        className={`form-control ${ (meta.touched && meta.error) ? 'is-invalid' : null}`}
      />
      { meta.touched && meta.error ? (
        <div className="invalid-feedback">{ meta.error }</div>
      ) : null }
    </div>
  )
}

export default TextField
