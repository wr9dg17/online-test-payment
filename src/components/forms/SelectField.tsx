import { Field, useField } from "formik"
import { FieldProps } from "../../shared/types/fieldProps"

const SelectField = (props: FieldProps) => {
  const [field, meta] = useField(props);

  return (
    <div className="form-group mb-2">
      { props.label && <label className="form-label">{ props.label }</label> }
      <Field
        {...field}
        {...props.rest}
        component="select"
        className={`form-select ${(meta.touched && meta.error) ? 'is-invalid' : null}`}
      >
        <option value="" label="Make choise" />
        { props.options && props.options.map(option => (
          <option value={option.k} label={option.v} key={option.k} />
        )) }
      </Field>
      { meta.touched && meta.error ? (
        <div className="invalid-feedback">{ meta.error }</div>
      ) : null }
    </div>
  )
}

export default SelectField
