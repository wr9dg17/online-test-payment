import { Receipt } from "../../../shared/interfaces"
import { Link } from "react-router-dom"
import Table from "react-bootstrap/Table"
import Button from "react-bootstrap/Button"

type PaymentResultProps = {
  payment: Receipt
}

const PaymentResult = ({ payment }: PaymentResultProps) => (
  <div>
    <Table striped bordered>
      <tbody>
        <tr>
          <td>ID</td>
          <td>{payment.id}</td>
        </tr>
        <tr>
          <td>Date</td>
          <td>{payment.date}</td>
        </tr>
        { payment.details.map(detail => (
          <tr key={detail.k}>
            <td>{ detail.k }</td>
            <td>{ detail.v }</td>
          </tr>
        )) }
        <tr>
          <td>Amount</td>
          <td>{payment.amount.value}</td>
        </tr>
        <tr>
          <td>Currency</td>
          <td>{payment.amount.currency}</td>
        </tr>
      </tbody>
    </Table>
    <Button variant="secondary" as={Link} to="/">Go back</Button>
  </div>
)

export default PaymentResult