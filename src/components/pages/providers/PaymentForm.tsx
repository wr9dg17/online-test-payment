import * as Yup from "yup"
import { Formik, Form } from "formik"
import { useState, useEffect } from "react"
import { TextField, SelectField } from "../../forms"
import { CustomField, Receipt } from "../../../shared/interfaces"
import { paymentAPI } from "../../../api"

import Row from "react-bootstrap/Row"
import Col from "react-bootstrap/Col"
import Button from "react-bootstrap/Button"

type PaymentFormProps = {
  providerName: string,
  providerId: string | number,
  fields: CustomField[],
  onPaymentSuccess: (data: Receipt) => void
}

const PaymentForm = ({
  providerName,
  providerId,
  fields = [],
  onPaymentSuccess
}: PaymentFormProps) => {
  const [formData, setFormData] = useState<any>({})
  const [validationSchema, setValidationSchema] = useState<any>({})

  useEffect(() => {
    initForm(fields)
  }, [providerId])

  const initForm = (fields: CustomField[]) => {
    let _schemaData: any = {}
    let _formData: any = {
      providerId: providerId,
      fields: {},
      amount: {
        currency: "AZ",
        value: ""
      },
      card: {
        number: "",
        exp_month: "",
        exp_year: "",
        cvv: ""
      }
    }

    fields.forEach(field => {
      _formData.fields[field.name] = "";
      if (field.type !== 4) {
        _schemaData[field.name] = Yup.string().required("Required")
      } else {
        // @ts-ignore
        _schemaData[field.name] = Yup.string().oneOf(field.options.map(o => o.k)).required("Required")
      }
    })

    setFormData(_formData)
    setValidationSchema(Yup.object({
      fields: Yup.object().shape({..._schemaData}),
      amount: Yup.object({
        value: Yup.string().required("Required")
      }),
      card: Yup.object({
        number: Yup.string().required("Required").length(16, "Must be 16 numbers"),
        exp_month: Yup.string().required("Required").length(2, "Must be 2 numbers"),
        exp_year: Yup.string().required("Required").length(2, "Must be 2 numbers"),
        cvv: Yup.string().required("Required").length(3, "Must be 3 numbers")
      })
    }))
  }

  return (
    <div>
      <p className="fs-3">{providerName}</p>
      {formData.providerId && (
        <Formik
          initialValues={formData}
          validationSchema={validationSchema}
          onSubmit={(values, { setSubmitting }) => {
            paymentAPI.makeNewPayment(values)
              .then(data => {
                onPaymentSuccess(data)
                setSubmitting(false)
              })
              .catch(error => {
                // TODO: handle error (notify user)
                console.log(error)
                setSubmitting(false)
              })
          }}
        >
          { formik => (
            <Form>
              {fields && fields.map(field => {
                return field.type !== 4 ? (
                  <TextField
                    key={field.id}
                    label={field.label}
                    name={`fields[${field.name}]`}
                  />
                ) : (
                  <SelectField
                    key={field.id}
                    label={field.label}
                    options={field.options}
                    name={`fields[${field.name}]`}
                  />
                )
              })}
              <TextField type="number" label="Amount (AZN)" name="amount[value]" />
              <TextField type="number" label="Card number" name="card[number]" />
              <Row>
                <Col lg={4}>
                  <TextField type="number" label="Exp. month" name="card[exp_month]" />
                </Col>
                <Col lg={4}>
                  <TextField type="number" label="Exp. year" name="card[exp_year]" />
                </Col>
                <Col lg={4}>
                  <TextField type="number" label="CVV" name="card[cvv]" />
                </Col>
              </Row>
              <div className="d-flex flex-column mt-2">
                <Button type="submit" variant="primary" disabled={formik.isSubmitting}>Submit</Button>
              </div>
            </Form>
          ) }
        </Formik>
      )}
    </div>
  )
}

export default PaymentForm
