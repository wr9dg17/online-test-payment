import { useState, useEffect } from "react"

export function useNetwork(): boolean {
  const [isOnline, setOnline] = useState<boolean>(window.navigator.onLine)

  const updateOnline = () => {
    setOnline(window.navigator.onLine);
  };

  useEffect(() => {
    window.addEventListener("offline", () => updateOnline)
    window.addEventListener("online", () => updateOnline)

    return () => {
      window.removeEventListener("offline", () => updateOnline)
      window.removeEventListener("online", () => updateOnline)
    }
  })

  return isOnline
}