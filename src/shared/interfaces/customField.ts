import Pair from "./pair"

export default interface CustomField {
  id: string | number,
  type: number,
  name: string,
  label: string,
  options?: Pair<any, any>[],
}