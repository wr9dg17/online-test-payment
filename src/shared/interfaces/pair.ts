export default interface Pair<T, U> {
  k: T,
  v: U
}