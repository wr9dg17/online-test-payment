export default interface PlasticCard {
  number: string,
  exp_month: string,
  exp_year: string,
  cvv: string
}