export default interface Amount {
  currency: "AZN" | "USD" | "EUR" | "RUB",
  value: string
}