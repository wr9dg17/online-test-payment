import CustomField from "./customField"

export default interface Provider {
  id: number | string,
  name: string,
  fields: CustomField[]
}