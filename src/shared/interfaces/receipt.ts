import Pair from "./pair";
import Amount from "./amount";

export default interface Receipt {
  id: string,
  date: string,
  details: Pair<any, any>[],
  amount: Amount
}