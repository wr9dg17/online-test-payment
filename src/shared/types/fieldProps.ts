import { Pair } from "../interfaces";

export type FieldProps = {
  name: string,
  type?: string,
  label?: string,
  placeholder?: string,
  options?: Pair<any, any>[],
  rest?: {}[]
}